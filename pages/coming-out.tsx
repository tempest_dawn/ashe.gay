import { ReactChild, useEffect, useState } from 'react'
import Post from '~/types/post'

import PostHead from '~/components/PostHead'
import { getRawPosts, resolvePostType } from '~/services/data'
import { useInView } from 'react-intersection-observer'

import styles from '~/styles/coming-out.module.css'

interface PropTypes {
  post : Post
}

export default function ComingOut({post} : PropTypes) {
  const [shouldStagger, setShouldStagger] = useState(true)
  useEffect(() => {
    const t = setTimeout(() => {
      setShouldStagger(false)
    }, 100)
  }, [])

  return (
    <>
      <PostHead post={post} />

      <Said>Oh, so you heard the news?</Said>
      <Thought shouldStagger={shouldStagger}>uh oh</Thought>

      <Said>Sorry, I&apos;ve kind of forgotten who knows and who doesn&apos;t yet.</Said>
      <Thought shouldStagger={shouldStagger}>also i dreaded telling you</Thought>

      <Said>No I don&apos;t know her sorry.</Said>
      <Thought shouldStagger={shouldStagger}>do you think i know every trans person in the state</Thought>

      <Said>That &mdash; that&apos;s not really how it works.</Said>
      <Thought shouldStagger={shouldStagger}>how do you even get to that conclusion</Thought>

      <Said>Yeah, it&apos;s going to take a long time.  I&apos;m excited though.</Said>
      <Thought>they seem to be coming around</Thought>

      <Said>My name?  Yeah I like it a lot.</Said>
      <Thought>oh no</Thought>

      <Said>No that&apos;s not why I picked it.</Said>
      <Thought>here we go again</Thought>

      <Said>No really, I don&apos;t want to hear the joke.</Said>
      <Thought>please stop</Thought>

      <Said>Sorry, I&apos;m not really comfortable talking about that.</Said>
      <Thought>i don&apos;t even know</Thought>

      <Said>Hey now, I didn&apos;t exactly lie, I just wasn&apos;t ready to tell the full truth.</Said>
      <Thought>why do i have to explain again</Thought>

      <Said>Some things yeah, but I&apos;m still the same person!</Said>
      <Thought>don&apos;t make me explain again</Thought>

      <Said>No that&apos;s not going to stop &mdash; I&apos;ll have to keep taking it for the rest of my life.</Said>
      <Thought>regretting explaining again</Thought>

      <Said>I don&apos;t think I&apos;m ready for that.</Said>
      <Thought>i&apos;m done with explaining again</Thought>

      <Said>I guess we&apos;ll just have to find out.</Said>
      <Thought>so tired of explaining again</Thought>

      <Said> . . . </Said>
      <Thought>so tired of explaining again</Thought>

      <Said/>
      <Thought>so tired of explaining again</Thought>

      <Said/>
      <Thought>so tired of explaining again</Thought>

      <Said/>
      <Thought className={styles.medium}>so tired</Thought>

      <Said/>
      <Thought className={styles.large}>of explaining</Thought>

      <Said/>
      <Thought className={styles.huge}>again</Thought>

      <div className={styles.space}/>

      <Said>What?  Sorry, I&apos;m a bit distracted.</Said>

      <Said>Oh!  Yeah . . . thanks.</Said>

      <Said>I&apos;m glad you asked too.</Said>

      <noscript>
        <style>{`.${styles.thought} {opacity: .6;}`}</style>
      </noscript>
    </>
  )
}

function Said({children} : {children?: ReactChild}) {
  if (children)
    return (
      <p className={styles.said}>
        &ldquo;{children}&rdquo;
      </p>
    )

  return <span className={styles.said}/>
}

function Thought({children, shouldStagger, className} : {children: ReactChild, shouldStagger?: boolean, className?: string}) {
  const { ref, inView } = useInView({
    triggerOnce: true,
    rootMargin: '-30%'
  })

  return (
    <aside ref={ref} className={`${styles.thought} ${inView && styles.visible} ${shouldStagger && styles.staggered} ${className || ''}`}>
      &lt; {children} &gt;
    </aside>
  )
}


export async function getStaticProps() : Promise<{props: PropTypes}>{
  const posts = await getRawPosts()
  const post = posts.find(post => post.path.join('/') === 'coming-out')


  return {
    props: {
      post: await resolvePostType(post)
    }
  }
}
