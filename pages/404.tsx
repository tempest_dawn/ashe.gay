import Head from 'next/head'
import PostComponent from '~/components/Post'

export default function DynamicPage(){
  return (
    <>
      <Head><title>Not Found | ashe.gay</title></Head>
      <h1>Not found</h1>
      <p>404</p>
    </>
  )
}