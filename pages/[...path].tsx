import Post, {postIsEncrypted} from '~/types/post'
import {getRawPosts, resolvePostType, processPostImages } from '~/services/data'

import PostComponent from '~/components/Post'

export default function PostPage({post} : {post : Post}) {
  return <PostComponent post={post} />
}

export async function getStaticPaths() {
  const posts = await getRawPosts()

  return {
    paths: posts
      .filter(post => post.body)
      .map(post => ({params: {path: post.path}})),
    fallback: false
  }
}

export async function getStaticProps({params: {path}}) {
  const posts = await getRawPosts()

  const postFile = posts.find(post => post.path.join('/') === path.join('/'))
  const postFileWithImages = await processPostImages(postFile)
  const post = await resolvePostType(postFileWithImages)

  return {
    props: {
      post
    }
  }
}
