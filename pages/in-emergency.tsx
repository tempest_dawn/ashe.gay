import {useState} from 'react'

import PostHead from '~/components/PostHead'
import Signature from '~/components/Signature'

import styles from '~/styles/emergency.module.css'

export default function EmergencyContact () {
  const [input, setInput] = useState('')
  const [status, setStatus] = useState(null)

  async function submit(ev) {
    if (ev) ev.preventDefault()

    setStatus(null)

    try {
      const response = await fetch('https://kae.tempest.dev/api/contact/ashe',{
        method: 'post',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          email: 'Not specified',
          name: 'Not specified',
          message: input
        })
      })

      if (response.status !== 200)
        throw new Error()

      setStatus('Message sent successfully')
    } catch {
      setStatus('Error sending message')
    }
  }

  function onChange(ev) {
    setInput(ev.target.value)
    setStatus(null)
  }

  return (
    <>
      <PostHead
        post={{
          date: '2022-07-19',
          title: 'In Case of Emergency',
          unlisted: false,
          path: ['in-emergency'],
          body: null,
          tags: []
        }}
      />

      <p>
        So I have a friend in a bit of an emergency situation and I have no
        direct way to contact them.  I&apos;m throwing this up as a sort of last-ditch
        effort to let them contact me if all other communication methods are
        broken.
      </p>

      <p>
        If you don&apos;t know what this is for, please ignore it.
      </p>

      <p>
        If you are that friend: this is a one-way message directly to me.
      </p>

      <textarea className={styles.textarea} value={input} onChange={onChange} />

      <button className={styles.button} type="submit" onClick={submit}>Send</button>

      {status && (
        <p className={styles.staus}>{status}</p>
      )}

      <p>
        I hope you&apos;re safe.  Wherever you are, just say the word and
        I will do whatever I can.  Please be careful.
        <i className="em em-purple_heart" style={{marginLeft: 8}} aria-role="presentation" aria-label="(purple heart)"></i> 
      </p>

      <Signature/>
    </>
  )
}
