import Head from 'next/head'
import Link from 'next/link'

import {getRawPosts} from '~/services/data'
import Post from '~/types/post'

import styles from '~/styles/posts.module.css'

export default function PostList({posts} : {posts: Post[]}) {
  return (
    <>
      <Head><title>Posts | ashe.gay</title></Head>
      <h1>posts</h1>
      {posts?.map(post => (
        <div key={post.path.join('/')} className={styles.post}>
          <span className={styles.date}>{(new Date(Date.parse(post.date)).toLocaleDateString('en-US', {timeZone: 'UTC'}))}</span>
          <div>
            <Link href={`/${post.path.join('/')}`}>
              <a className={styles.name}>{post.title}</a>
            </Link>
            <span className={styles.tags}>
              {post.tags.map(tag => (
                <span key={tag} className={styles.tag}>{tag}</span>
              ))}
            </span>
          </div>
        </div>
      ))}
    </>
  )
}

export async function getStaticProps() {
  const posts = await getRawPosts()

  return {
    props: {
      posts: posts.filter(post => !post.unlisted)
    }
  }
}
