import AES from 'crypto-js/aes'
import Utf8 from 'crypto-js/enc-utf8'

import {UnencryptedPost, EncryptedPost} from '~/types/post'

export function encryptPost(password : string, passwordHint : string, post : UnencryptedPost) : EncryptedPost {
  const encryptedBlob = AES.encrypt(post.body, password).toString()

  return {
    date: post.date,
    title: post.title,
    path: post.path,
    prefix: post.prefix,
    unlisted: post.unlisted,
    tags: post.tags,
    passwordHint,
    encryptedBlob
  }
}

export function decryptPost(password: string, post : EncryptedPost) : UnencryptedPost | null {
  const body = AES.decrypt(post.encryptedBlob, password).toString(Utf8)

  if(!body)
    return null

  return {
    date: post.date,
    title: post.title,
    path: post.path,
    prefix: post.prefix,
    unlisted: post.unlisted,
    tags: post.tags,
    body
  }
}
