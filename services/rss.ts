import {Feed, Item} from 'feed'
import {getRawPosts} from '~/services/data'
import removeMarkdown from 'remove-markdown'
import {promises as fs} from 'fs'
import path from 'path'

export default async function generateRssFeed() {
  const posts = await getRawPosts()

  const baseUrl = 'https://ashe.gay'
  const author = {
    name: 'Ashe',
    email: 'ashe@tempest.dev',
    link: 'https://twitter.com/ashen_dawn',
  }

  // Construct a new Feed object
  const feed = new Feed({
    title: 'ashe.gay',
    description: '',
    id: baseUrl,
    link: baseUrl,
    language: 'en',
    feedLinks: {
      rss2: `${baseUrl}/rss.xml`,
    },
    author,
    copyright: `All rights reserved ${new Date().getFullYear()}, Ashe`
  })

  // Add each article to the feed
  for(const post of posts) {
    if(post.unlisted)
      continue

    const feedItem : Item = {
      title: post.title,
      link: `${baseUrl}/${post.path}`,
      date: new Date(Date.parse(post.date))
    }

    if(!post.password) {
      feedItem.content = removeMarkdown(post.body)
    } else {
      feedItem.content = 'Content for this post is password-protected'
    }

    if(feedItem.content.trim() === '') {
      feedItem.content = 'This post uses animations or other content that cannot be previewed effectively'
    }

    feed.addItem(feedItem)
  }

  try {
    await fs.mkdir(path.join(process.cwd(), 'public'))
  } catch (err) {
    if(err.code !== 'EEXIST')
      throw err
  }
  await fs.writeFile(path.join(process.cwd(), 'public/rss.xml'), feed.rss2())
}