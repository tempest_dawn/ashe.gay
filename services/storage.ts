import Post, { UnencryptedPost, EncryptedPost } from '~/types/post'

interface SessionStorageRecord {
  encryptedBlob: string,
  post: UnencryptedPost
}

function getKeyFromPost(post : Post) {
  return `ashe_postcache_${post.path}`
}

export function loadPost(post : EncryptedPost) : UnencryptedPost | void {
  if (typeof window === 'undefined')
    throw new Error('Storage service cannot be called from server')

  try {
    const record : SessionStorageRecord = JSON.parse(window.sessionStorage.getItem(getKeyFromPost(post)))

    if (record.encryptedBlob !== post.encryptedBlob)
      window.sessionStorage.removeItem(getKeyFromPost(post))

    return record.post
  } catch {}
}

export function storePost(post : UnencryptedPost, encryptedBlob : string) {
  if (typeof window === 'undefined')
    throw new Error('Storage service cannot be called from server')

  const record : SessionStorageRecord = {
    encryptedBlob,
    post
  }

  window.sessionStorage.setItem(getKeyFromPost(post), JSON.stringify(record))
}