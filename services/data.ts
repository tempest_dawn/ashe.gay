const {promises: fs} = require('fs')
const fsPath = require('path')
const sharp = require('sharp')
var parseMarkdown = require('front-matter-markdown')

import { encryptPost } from './crypt'
import Post, {PostFile, UnencryptedPost} from '~/types/post'
import { BaseImage, ImageWithPreview } from '~/components/Image'

export async function getRawPosts() : Promise<PostFile[]> {
  const postsDir : string = fsPath.join(process.cwd(), 'posts')
  const filePaths : string[] = await fs.readdir(postsDir)

  const files : {filename: string, text: string}[] = await Promise.all(filePaths.map(async file => ({filename: file.replace('.md', ''), text: await fs.readFile(fsPath.join(process.cwd(), 'posts', file))})))

  const parsedPosts : PostFile[] = await Promise.all(files.map(async ({filename, text}) => {
    const tempObj = parseMarkdown(text)
    const {date, index, path: filePath} = extractDateFromFilename(filename)

    let path = [filePath]
    if (tempObj.prefix && typeof tempObj.prefix === 'string')
      path = [tempObj.prefix, filePath]

    return {
      date,
      index,
      path,
      tags: tempObj.tags || [],
      unlisted: tempObj.unlisted || false,
      draft: tempObj.draft || false,
      title: tempObj.title,
      body: tempObj.content || null,
      prefix: tempObj.prefix || null,
      password: tempObj.password || null,
      passwordHint: tempObj.passwordHint || null
    }
  }))

  parsedPosts.sort((postA, postB) => {
    const dateA = new Date(Date.parse(postA.date))
    const dateB = new Date(Date.parse(postB.date))

    // Later date first
    if (dateA > dateB)
      return -1
    else if (dateB > dateA)
      return 1

    // Lower index first
    if (postA.index < postB.index)
      return -1
    else if (postB.index < postA.index)
      return 1
    else
      return 0
  })

  return parsedPosts.filter(post => !post.draft)
}

export async function processPostImages(post : PostFile) : Promise<PostFile> {
  if (post.body) {
    const imgRegex = /\!\[([^\]]+)\]\(([^\)]+)\)/g
    let imageReplacements = []
    post.body.replace(imgRegex, (imageExp : string, title : string, url : string) => {
      imageReplacements.push((async () => {
        try {
          const baseImage = {
            alt: title,
            title: title,
            src: url
          }

          const imageData = await processImage(baseImage)

          return (`<Image
            src="${imageData.src}"
            alt="${imageData.alt}"
            title="${imageData.title}"
            thumbnailUrl="${imageData.thumbnailUrl}"
            thumbnailWidth={${imageData.thumbnailWidth}}
            thumbnailHeight={${imageData.thumbnailHeight}}
            previewData="${imageData.previewData}"
            aspectRatio={${imageData.aspectRatio}}
          />`).replace(/[\n ]+/g, ' ')
        } catch (e) {
          console.log(`Error processing image ${url}: ${e.message} (${e.stack?.split('\n')[1].trim()})`)
          return imageExp
        }
      })())
      return ''
    })

    const resultSnippets = await Promise.all(imageReplacements)
    post.body = post.body.replace(imgRegex, () => resultSnippets.shift())
  }

  return post
}

export async function resolvePostType(postFile : PostFile) : Promise<Post> {
  const post : UnencryptedPost = {
    date: postFile.date,
    title: postFile.title,
    path: postFile.path,
    body: postFile.body || null,
    tags: postFile.tags,
    prefix: postFile.prefix,
    unlisted: postFile.unlisted
  }

  if(postFile.password)
    return encryptPost(
      postFile.password,
      postFile.passwordHint,
      post
    )

  return post
}

function extractDateFromFilename(filename: string) : {date?: string, index?: number, path: string} {
  const [dateComponent, ...pathComponents] = filename.split('_')

  if(!/[0-9]{4}-[0-9]{2}-[0-9]{2}(-[0-9]{2}|)/.test(dateComponent)) {
    console.log('Could not extract date from '+ dateComponent)
    return {path: filename}
  }

  const path = pathComponents.join('_')
  const [year, month, day, indexString = '0'] = dateComponent.split('-')
  const date = [year, month, day].join('-')
  const index = parseInt(indexString, 10)
  return {date, index, path}
}

async function processImage(imageData : BaseImage, thumbnailWidth : number = 600, fullSizeDimension : number = 800) : Promise<ImageWithPreview> {
  const imgPath = imageData.src

  if (imgPath?.[0] !== "/" || imgPath?.[1] === "/") {
    throw new Error("processImage: Could not generate thumbnails - not a local absolute path")
  }

  if (/\.gif$/.test(imgPath)) {
    throw new Error("processImage: Will not generate thumbnail for gif")
  }
  const publicDir = fsPath.join(process.cwd(), 'public')
  const inputPath = fsPath.join(publicDir, imgPath)

  const image = await sharp(inputPath).rotate()
  const metadata = await image.metadata()

  const origFilename = imgPath.split('/').slice(-1)[0].split('.')[0]
  const randPrefix = Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 5)
  const outputName = `${randPrefix}-${origFilename}`
  const thumbnailFile = `/images/generated/${outputName}-thumb.jpg`
  const imageFile = `/images/generated/${outputName}-large.${metadata.format}`


  const landscape = metadata.width > metadata.height
  const smallestDimension = landscape ? 'height' : 'width'

  const [preview, thumbnail] = await Promise.all([
    image.clone().resize({[smallestDimension]: 16}).jpeg({quality: 50}).toBuffer(),
    image.clone().resize({width: thumbnailWidth}).jpeg({quality: 70}).toFile(publicDir + thumbnailFile),
    image.clone().resize({[smallestDimension]: fullSizeDimension}).toFile(publicDir + imageFile)
  ])

  return {
    ...imageData,
    aspectRatio: metadata.width / metadata.height,
    previewData: `data:image/jpeg;base64,${preview.toString('base64')}`,
    thumbnailUrl: thumbnailFile,
    thumbnailHeight: thumbnail.height,
    thumbnailWidth: thumbnail.width
  }
}
