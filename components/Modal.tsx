import {createPortal} from 'react-dom'
import {useState, useEffect, useRef, MouseEvent} from 'react'

import styles from '~/styles/modal.module.css'

interface ModalProps {
  children: JSX.Element,
  onClose: Function
}

export default function Modal({children, onClose} : ModalProps) {
  const [element] = useState(() => document.createElement('div'))
  const [closing, setClosing] = useState(false)
  const container = useRef<HTMLDivElement>()

  useEffect(() => {
    const container = document.getElementById('modalContainer')
    container.appendChild(element)
    return () => { container.removeChild(element) }
  }, [])

  function closeModal(ev : MouseEvent<HTMLDivElement>) {
    if(container.current.contains(ev.target as Node))
      return;

    setClosing(true)
    setTimeout(() => {
      onClose()
    }, 200)
  }

  return createPortal(
    <div className={`${styles.modalBackground} ${closing ? styles.closing : ''}`} onClick={closeModal}>
      <div className={styles.modalContainer} ref={container}>
        {children}
      </div>
    </div>,
    element
  )
}