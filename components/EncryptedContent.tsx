import type {EncryptedPost} from '~/types/post'
import { useState } from 'react'

import usePostDecryptor from '~/hooks/usePostDecryptor'
import useScrollbarWidth from '~/hooks/useScrollbarWidth'
import Markdown from './Markdown'

export default function EncryptedContent({post} : {post : EncryptedPost}) {
  const [password, setPassword] = useState('')
  const [decryptedPost, attemptPassword] = usePostDecryptor(post)

  useScrollbarWidth()

  if(decryptedPost)
    return (
      <Markdown body={decryptedPost.body}/>
    )

  function decrypt(ev) {
    ev.preventDefault()

    attemptPassword(password)
  }

  return (
    <>
      <div className="encryptedContent">
        <div aria-hidden="true" role="presentation" className="contentBackground">
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam quis nisi lacus. Curabitur lacinia mattis risus, a hendrerit turpis commodo sed. Mauris aliquam dui a sapien efficitur tempus. Ut commodo tincidunt semper. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin a est dignissim leo mollis suscipit non ac felis. Integer tincidunt eros a finibus fringilla.</p>
          <p>Nulla dignissim ultrices augue non pellentesque. Ut egestas imperdiet euismod. Cras non libero nec purus scelerisque condimentum accumsan nec purus. Nam tristique, velit posuere gravida pretium, diam arcu ullamcorper ligula, vel commodo quam orci at sapien. Nam varius vitae mauris a viverra. Quisque nec nunc neque. Duis a velit imperdiet, egestas sem nec, elementum mauris. Donec rhoncus nulla tortor, vel tristique orci elementum eu. Suspendisse fringilla massa non nunc semper placerat id ut nibh. Pellentesque in blandit odio. Nullam aliquet lacus nec eros ornare viverra.</p>
          <p>Praesent vitae velit lectus. Praesent vel lorem dui. Nullam ac orci consequat, suscipit tellus ut, mattis arcu. Sed pretium eros et dignissim luctus. Aliquam erat volutpat. Integer viverra metus lacus, sit amet viverra leo luctus quis. Sed vel ipsum nibh. Integer tortor elit, commodo sed facilisis varius, porta ac eros. Nunc ut arcu sem. Ut non sem est. Praesent ultricies, ipsum quis dignissim sollicitudin, nisi arcu tincidunt lectus, id fermentum tortor nisl quis diam.</p>
          <p>Donec lectus sem, iaculis eu consectetur sed, tincidunt non neque. Etiam non elit tristique, commodo orci quis, iaculis sem. Sed id convallis massa. Cras lobortis massa quis turpis venenatis, quis mattis ipsum feugiat. Quisque ex nulla, porttitor tempor diam vel, porta egestas nunc. Pellentesque eget ullamcorper magna, ut aliquam tortor. Maecenas congue ex a nisl dignissim, id hendrerit purus dignissim. Quisque iaculis tristique erat a mollis. Donec volutpat orci mauris, ac efficitur diam venenatis eu. Nunc nisl nisl, imperdiet quis purus vitae, finibus pulvinar nisl. Fusce ultricies scelerisque enim a auctor. Aliquam aliquam, lectus et malesuada tempor, est nunc condimentum nulla, nec posuere nisi enim sit amet dolor. Suspendisse vitae hendrerit quam.</p>
          <p>Duis tristique, augue ut iaculis pretium, nulla purus euismod ante, tincidunt lacinia metus lectus eu massa. Fusce congue velit vel velit egestas, non viverra massa fermentum. Ut tempor ornare odio ut scelerisque. Proin tempus faucibus dui a semper. Pellentesque ligula magna, ultrices eget tortor gravida, volutpat pellentesque urna. Sed ac ullamcorper nunc. Fusce maximus eros at tortor sodales commodo. Integer et porta ipsum, id pretium purus. Fusce eget diam a nunc cursus aliquam. Phasellus ultrices lacinia ipsum et facilisis. In mollis odio mi, nec laoreet lorem laoreet sit amet. Sed id nisi in metus facilisis cursus. </p>
        </div>
        <div aria-hidden="true" role="presentation" className="encryptedBanner"/>
        <form onSubmit={decrypt}>
          <p>This post&apos;s content is encrypted<br/>please enter the password to open it</p>
          <p><input type="password" value={password} onChange={ev => setPassword(ev.target.value)}/></p>
          <p><button type="submit">Decrypt</button></p>
        </form>
      </div>
    </>
  )
}