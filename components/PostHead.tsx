import Head from 'next/head'
import Link from 'next/link'
import Post from '~/types/post'

interface PropTypes {
  post : Post
}

export default function PostHead({post} : PropTypes) {
  const date = new Date(Date.parse(post.date + 'T00:00:00Z'))
  const dateString = date.toLocaleDateString('en-US', {
    timeZone: 'UTC'
  })

  return (
    <>
      <Head><title>{post.title} | ashe.gay</title></Head>
      {post.prefix
        ? <Link href={`/${post.prefix}`}><a>{'<'} back</a></Link>
        : <Link href="/posts"><a>{'<'} see all posts</a></Link>
      }
      <h1>{post.title}</h1>
      <span className="date">{dateString}</span>
    </>
  )
}
