import SignatureSvg from '~/images/signature.svg'

import styles from '~/styles/signature.module.css'

export default function Signature() {
  return (
    <div className={styles.signature}>
      <span className={styles.tilde}>~</span>
      <SignatureSvg height="auto"/>
    </div>
  )
}