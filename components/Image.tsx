import { CSSProperties, useEffect, useState, MouseEvent } from 'react'
import { useInView } from 'react-intersection-observer'
import styles from '~/styles/image.module.css'
import Modal from './Modal';

export interface BaseImage {
  alt: string,
  src: string,
  title?: string
  thumbnailUrl?: string
}

interface ImageWithThumbnail extends BaseImage {
  thumbnailWidth: number,
  thumbnailHeight: number
}

export interface ImageWithPreview extends ImageWithThumbnail {
  previewData: string,
  aspectRatio: number
}

export default function Image(props : BaseImage | ImageWithPreview) {
  const {alt, src, thumbnailUrl, title} = props;

  const hasPreview = "previewData" in props;
  const [imageLoaded, setImageLoaded] = useState(hasPreview ? false : true)
  const [modalShown, setModalShown] = useState(false)

  const {ref, inView} = useInView({triggerOnce: true})

  useEffect(() => {
    if(!hasPreview || !inView)
      return

    const img = new window.Image()
    img.onload = () => setImageLoaded(true)
    img.src = thumbnailUrl || src
  }, [hasPreview, inView, thumbnailUrl, src])

  if(!hasPreview) {
    return (
      <a href={src} target="_blank">
        <img alt={alt} src={thumbnailUrl || src} title={title}/>
      </a>
    )
  }

  const {thumbnailWidth, thumbnailHeight, previewData, aspectRatio} = props as ImageWithPreview;

  const style : CSSProperties = {
    width: thumbnailWidth + 'px',
    height: thumbnailHeight + 'px',
    maxWidth: 'calc(100vw - 100px)',
    maxHeight: `calc((100vw - 100px) / ${aspectRatio})`
  }

  function onClick(ev : MouseEvent<HTMLAnchorElement>) {
    ev.preventDefault();
    setModalShown(true);
  }

  return (
    <>
      <a ref={ref} onClick={onClick} style={style} className={styles.container} href={src} target="_blank">
        <img className={styles.preview} alt={alt} src={previewData} title={title}/>
        {imageLoaded && <img className={styles.final} alt={alt} src={thumbnailUrl || src} title={title}/>}
        <noscript>
          {/* <style>{`.${styles.preview} { display: none; }`}</style> */}
          <img className={styles.final} alt={alt} src={thumbnailUrl || src} title={title}/>
        </noscript>
      </a>
      {modalShown && (
        <Modal onClose={() => setModalShown(false)}>
          <img style={{display: 'block', maxWidth: '100%', maxHeight: '100%', margin: '0 auto'}} src={src}/>
        </Modal>
      )}
    </>
  )
}