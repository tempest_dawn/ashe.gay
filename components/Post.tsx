import Post, {postIsEncrypted} from '~/types/post'

import EncryptedContent from './EncryptedContent'
import Markdown from './Markdown'
import PostHead from './PostHead'

export default function PostComponent({post} : {post : Post}) {
  return (
    <>
      <PostHead post={post} />
      {postIsEncrypted(post) ? (
        <EncryptedContent post={post}/>
      ) : (
        <Markdown body={post.body}/>
      )}
    </>
  )
}