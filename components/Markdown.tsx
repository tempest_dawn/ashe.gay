import MarkdownJSX from 'markdown-to-jsx'

import Image from './Image'
import Link from './Link'
import Signature from './Signature'

export default function Markdown({body}) {
  const options = {
    overrides: {
      img: Image,
      a: Link,
      Signature: Signature,
      Image: Image
    }
  }

  return (
    <MarkdownJSX options={options}>
      {body}
    </MarkdownJSX>
  )
}