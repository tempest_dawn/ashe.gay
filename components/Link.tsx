import NextLink from 'next/link'

interface LinkProps {
  href: string,
  children: JSX.Element
}

export default function Link({href, children} : LinkProps) {
  const isLocal = /^\/[^\/]/.test(href)

  if (isLocal)
    return (
      <NextLink href={href}>
        <a>
          {children}
        </a>
      </NextLink>
    )

  return (
    <a
      href={href}
      target="blank"
      rel="noreferer noopener"
    >
      {children}
    </a>
  )
}
