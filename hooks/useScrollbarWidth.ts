import { useEffect } from 'react'

export default function useScrollbarWidth() {
  useEffect(() => {
    function calculateWidth() {
      document.documentElement.style.setProperty('--scrollbar-width', (window.innerWidth - document.documentElement.clientWidth) + 'px')
    }

    window.addEventListener('resize', calculateWidth, false)
    document.addEventListener('DOMContentLoaded', calculateWidth, false)
    window.addEventListener('load', calculateWidth)

    return () => {
      window.removeEventListener('resize', calculateWidth)
      document.removeEventListener('DOMContentLoaded', calculateWidth)
      window.removeEventListener('load', calculateWidth)
      document.documentElement.style.removeProperty('--scrollbar-width')
    }
  })
}