import { UnencryptedPost, postIsEncrypted, EncryptedPost } from '~/types/post'

import {loadPost, storePost} from '~/services/storage'
import { useEffect, useState, useCallback } from 'react'

export default function usePostDecryptor(post : EncryptedPost) : [UnencryptedPost | null, (string) => Promise<boolean> ] {
  const [decryptedPost, setDecryptedPost] = useState<UnencryptedPost | null>(null)

  // Check for decrypted post in cache
  useEffect(() => {
    const decryptedPost = loadPost(post)

    if(decryptedPost)
      setDecryptedPost(decryptedPost)
  }, [post])

  // Callback to make password attempt
  const attemptPassword = useCallback(async (password: string) => {
    const {decryptPost} = await import('~/services/crypt')

    const decryptedPost = decryptPost(password, post)
    if(decryptedPost) {
      storePost(decryptedPost, post.encryptedBlob)
      setDecryptedPost(decryptedPost)
      return true
    }

    return false
  }, [post])

  // Check if its encrypted to start with
  if (!postIsEncrypted(post))
    return [post, attemptPassword]

  if (decryptedPost) {
    return [decryptedPost, attemptPassword]
  }

  // Return null + callback
  return [null, attemptPassword]
}