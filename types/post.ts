interface PostCore {
  date: string,
  title: string,
  path: string[],
  prefix?: string,
  unlisted: boolean,
  tags: string[]
}

export type UnencryptedPost = PostCore & {
  body: string
}

export type EncryptedPost = PostCore & {
  passwordHint?: string
  encryptedBlob: string
}

export type PostFile = UnencryptedPost & {
  passwordHint?: string
  index?: number
  password?: string
  draft: boolean
}

export function postIsEncrypted(post : Post) : post is EncryptedPost {
  if((post as EncryptedPost).encryptedBlob)
    return true
  return false
}

type Post = EncryptedPost | UnencryptedPost
export default Post
