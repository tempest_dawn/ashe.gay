---
title: Fragments of My Heart
tags:
- personal life
- poem
---

i'll see you soon<br/>
in this messy dance we call life<br/>
floating through your orbit<br/>
passing in the night

i gave you something<br/>
before you left<br/>
a secret trust from deep inside<br/>
this place within my chest

so keep it safe<br/>
'til next we meet<br/>
and in your heart<br/>
my love will beat<br/>

then as i turned<br/>
and walked away<br/>
i found with me<br/>
your heart did stay

so i shall hold it<br/>
safe and dear<br/>
until once more<br/>
you come 'round here

i'll see you soon<br/>
in this messy dance we call life<br/>
floating through your orbit<br/>
passing in the night

