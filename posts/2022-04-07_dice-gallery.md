---
title: Dice Gallery
unlisted: true
---

This is a quick gallery of some of the dice images I've made over the last few years.  These were all modeled in Blender, and rendered in Cycles.

The first I did just to experiment with the glass shader and volumetrics - kind of fun, I didn't really have the lighting down yet though:
![Initial Unfinished Die](/images/dice/unfinished.png)

Next I played around with a few different volumetric effects - both with and without particles:
![Trans-Final](/images/dice/Trans-Final.png)
![Ice](/images/dice/Ice.png)
![Radioactive](/images/dice/Radioactive.png)

I tried doing some stuff on the surface, but without modeling different pieces it really didn't work well:
![Lava](/images/dice/Lava.png)

And then I tried putting objects \*in\* the dice - these turned out really well I think:
![CherryTree](/images/dice/CherryTree.png)
![Skull](/images/dice/Skull.png)
(note: I did not model this skull - it's a CC0 model I found somewhere)

And more recently I've experimented a bit with emulating different materials - this silicone-ish one is the best of the lot:
![Silicone](/images/dice/Silicone.png)
