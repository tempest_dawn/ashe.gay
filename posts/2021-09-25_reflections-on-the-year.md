---
title: Reflections On the Last Year
tags:
- personal life
---

So a year ago today my wife and I moved to our current home.  For the first time in several years we weren't living with family, and in many ways that move signalled the beginning of my transition.

A year ago I came out to my wife, a year ago I walked away from the mormon church, and a year ago I started rebuilding my life.

Since then I've found a new job, reconnected with several old friends, and my wife and I are expecting another baby.  It turns out life goes on, even after everything changes.

Of course everything hasn't been all pleasant.  I've had family members tell me they're worried I'll go to hell, old friends insist I'm delusional, and of course the constant strangers online calling me and people like me freaks.  I learned to deal with the strangers a long time ago, but it has hurt more coming from people I trusted and loved.

Still, better to know how they feel than to be forever unsure I guess.

I just wish they could climb into my head and see the world from my perspective.  I'm trying to do the best I can - both for myself and my family - and can only assume others are doing so as well.  Why can't they believe that's why I chose to transition?

I try not to dwell on that too much.

Instead I've found myself looking forward more than ever.  Transitioning has given me more hope for the future than I've had at any point in my life.  Goals suddenly make sense to have, and to work towards.  Obviously many are about my medical transition, but even beyond that I've been able to visualize in much greater detail how I want to take care of my health, the sort of progression I want in my career, and how I want to raise my children.  Turns out life is much clearer when you're not struggling to imagine a future for yourself.

To those of you who have believed me, thank you.  I'm still figuring a lot of things out, but it means a lot to me that you can trust I'm making the right choice for myself and my family.