---
title: Respite
tags:
 - poem
 - healing
---

in a world of bright faces<br/>
wide smiles and sunny hellos,<br/>
i am wind that brings in the clouds<br/>
to offer shade below

i am the touch of a sudden chill<br/>
to heal your sun-scorched day,<br/>
the beautiful smell of coming rain<br/>
to wash the grit away

i am the sound of a bubbling spring<br/>
just up around the bend,<br/>
a place to rest and catch your breath<br/>
as the day comes to an end

we'll clear the sky to sit together<br/>
and watch the moon rise high,<br/>
<span class="preserveCase">She'll</span> bless us both and off we'll go<br/>
with rested wings to fly

