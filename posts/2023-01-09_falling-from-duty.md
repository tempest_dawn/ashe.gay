---
title: Falling from Duty
tags:
- creative writing
- empty spaces
---

<div className="preserveCase"/>

You wake up from a restless right of empty dreams, sweating and shivering. The sky is dark but you know any chance at rest vanished the moment you opened your eyes.

The bed is sticky when you get up, riddled with bloody feathers. Your wings are molting. They started the day you discovered the tiny horns peeking out from under your hair, and haven't stopped since. You expect it will get worse for long before it gets better in that regard.

Taking the time to polish your halo used to fill you with confidence, you like to think you took pride in your work at least. Now the gesture feels empty. At the very least when your division lead comes to relieve you of your post they'll get it back shiny and scratch free, although given how the glow has faded not even you could claim it's been truly well cared for.

You return your halo polish to its place behind the mirror and frown when you catch sight of the horns.  What started as two small nubs, barely visible through your hair, have grown into long, shockingly elegant antlers, gently twisting their way around your head from the fore to just over your ears.

While you'll curse the day you discovered them for the rest of your eternities, you do have to admit they've been growing on you - even if only literally.

An idle thought passes its way through your head: do demons' horns have to be polished ... or perhaps oiled maybe? You'll have to find someone to ask about that. Luckily the humans can't seem to see your horns any more than they could see your wings - seems that trick runs both ways.

The sharp rap of a knock at the door interrupts your morning routine.

On opening the door you find an unmarked envelope, containing a handwritten memo:

> **Intake and Orientation**<br/>
> **Fallen Angel Division**<br/>
> **Hell**
> 
> Dearest new sibling,
> 
> Congratulations on your demotion.  This letter is to inform you
> of your upcoming assignment.
>
> Upon release from Heaven, your new duty will be:
> <div style="text-align:center">***Unspecified***</div>
>
> Enjoy the change of pace.

So the rumors were true. You feel your heart sink as it sets in. For the first time in your existence you would have no purpose. No duty. No meaning.

This really is hell.

