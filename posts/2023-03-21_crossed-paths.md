---
title: Crossed Paths
tags:
 - poem
 - love
 - departure
 - friends
---

though life will take us to and fro<br/>
while dancing all the way,<br/>
we find sometimes someone to know<br/>
and by them we will stay

a love, a spark, a needed help<br/>
to cherish and endure,<br/>
someone who keeps watch o'er yourself<br/>
in moments insecure

we dare not hope, we cannot know<br/>
how long these ones will stay,<br/>
but in our heart we hope to go<br/>
together on our way

and yet alas, how life does scorn<br/>
the dreams in hearts we keep,<br/>
we know sometimes we come to mourn<br/>
the future not to be

yet do not futures come and die<br/>
each moment of our life?<br/>
one cannot look o'er low or high<br/>
without some mortal strife

much better then ourselves to count<br/>
among the lucky ones,<br/>
that need not journey all around<br/>
while solely on our own

so if goodbye is come today<br/>
one thing i'll say is true<br/>
of all the folks, that crossed my way<br/>
i'm glad that i met you

