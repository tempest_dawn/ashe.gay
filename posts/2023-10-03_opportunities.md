---
title: Opportunities
prefix: ourtober-2023
unlisted: true
---

<span class="preserveCase"/>
<span class="preserveSpace"/>

In the twilight tail of life
Regret stabbing like a knife
At the end of mortal strife
Squandered out of fear

Think upon the beaten path
Moments one refrained to laugh
Notes marked down in life's great staff
That we didn't hear

When we leave this world alone
Will we be remembered, known?
Will we leave but crumbled bone
To those we hold dear

