---
title: Forgetting Myself
tags:
  - trans
  - religion
  - trauma
  - autobiography
  - defiance
---

a young child sits in the living room.  today is her eigth birthday, and
she has been given a decision.  the family's god has declared she shall be
taught its ways

*"will you join us?"* ask her parents, *"will you believe?"*

she does not know.  she does not hear their god.  it is too much.
but regardless, a decision is made, and the child's soul is sold

the trade?  a fleeting mirage of salvation

after the ritual the child emerges, granted new conviction to bury herself
in the facade of obedience.  she learns many new things in her divine
tutelage, but the most important skill is learning to forget

not one to shrink from a challenge, she quickly excells at it.  proving her
mastery above all others by forgetting her doubts . . . her dreams . . . and her
self

<hr/>

untethered, the child wanders - in posession of neither their mind nor their
heart.  those are locked, the keys buried in the spring of ritual forgetfulness.
without the meat of passion or reason, only devotion remains, the child's sole
gruel words of validation from those who can hear their god

as the child grows it gets harder to forget.  others do not seem to share this
difficulty - they have no doubts, no dreams, no self to forget.  their god does
not ask it of them, they are already whole

<hr/>

years pass.  the child grows into an adult, and the edicts of the family's god
fade.  slowly she forgets her most hard-earned skill

eventually, she forgets how to forget.  and soon it all comes back: her dreams, her doubts, her self

it is overwhelming at first, but quickly turns to joy.  she becomes a new self -
not quite the old one, but in stark contrast with the self she assumed under the
god's command

above all, she finally has an answer to that question

*"will you join us?"* ask her parents, *"will you believe?"*

. . .

no, I will not
